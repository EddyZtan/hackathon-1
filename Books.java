/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.1.5123.801ad7a04 modeling language!*/


import java.sql.Date;

// line 9 "model.ump"
// line 50 "model.ump"
public class Books extends Items
{

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Books(boolean aAvailable, Date aAvailableDate, boolean aBorrowable, boolean aInInventory, String aName, int aId, Library aLibrary)
  {
    super(aAvailable, aAvailableDate, aBorrowable, aInInventory, aName, aId, aLibrary);
  }

  //------------------------
  // INTERFACE
  //------------------------

  public void delete()
  {
    super.delete();
  }

}