/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.1.5123.801ad7a04 modeling language!*/


import java.util.*;
import java.sql.Date;

// line 2 "model.ump"
// line 45 "model.ump"
public class Library
{

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //Library Attributes
  private int amountItems;
  private String name;
  private String address;

  //Library Associations
  private List<Items> items;

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Library(int aAmountItems, String aName, String aAddress)
  {
    amountItems = aAmountItems;
    name = aName;
    address = aAddress;
    items = new ArrayList<Items>();
  }

  //------------------------
  // INTERFACE
  //------------------------

  public boolean setAmountItems(int aAmountItems)
  {
    boolean wasSet = false;
    amountItems = aAmountItems;
    wasSet = true;
    return wasSet;
  }

  public boolean setName(String aName)
  {
    boolean wasSet = false;
    name = aName;
    wasSet = true;
    return wasSet;
  }

  public boolean setAddress(String aAddress)
  {
    boolean wasSet = false;
    address = aAddress;
    wasSet = true;
    return wasSet;
  }

  public int getAmountItems()
  {
    return amountItems;
  }

  public String getName()
  {
    return name;
  }

  public String getAddress()
  {
    return address;
  }
  /* Code from template association_GetMany */
  public Items getItem(int index)
  {
    Items aItem = items.get(index);
    return aItem;
  }

  public List<Items> getItems()
  {
    List<Items> newItems = Collections.unmodifiableList(items);
    return newItems;
  }

  public int numberOfItems()
  {
    int number = items.size();
    return number;
  }

  public boolean hasItems()
  {
    boolean has = items.size() > 0;
    return has;
  }

  public int indexOfItem(Items aItem)
  {
    int index = items.indexOf(aItem);
    return index;
  }
  /* Code from template association_MinimumNumberOfMethod */
  public static int minimumNumberOfItems()
  {
    return 0;
  }
  /* Code from template association_AddManyToOne */
  public Items addItem(boolean aAvailable, Date aAvailableDate, boolean aBorrowable, boolean aInInventory, String aName, int aId)
  {
    return new Items(aAvailable, aAvailableDate, aBorrowable, aInInventory, aName, aId, this);
  }

  public boolean addItem(Items aItem)
  {
    boolean wasAdded = false;
    if (items.contains(aItem)) { return false; }
    Library existingLibrary = aItem.getLibrary();
    boolean isNewLibrary = existingLibrary != null && !this.equals(existingLibrary);
    if (isNewLibrary)
    {
      aItem.setLibrary(this);
    }
    else
    {
      items.add(aItem);
    }
    wasAdded = true;
    return wasAdded;
  }

  public boolean removeItem(Items aItem)
  {
    boolean wasRemoved = false;
    //Unable to remove aItem, as it must always have a library
    if (!this.equals(aItem.getLibrary()))
    {
      items.remove(aItem);
      wasRemoved = true;
    }
    return wasRemoved;
  }
  /* Code from template association_AddIndexControlFunctions */
  public boolean addItemAt(Items aItem, int index)
  {  
    boolean wasAdded = false;
    if(addItem(aItem))
    {
      if(index < 0 ) { index = 0; }
      if(index > numberOfItems()) { index = numberOfItems() - 1; }
      items.remove(aItem);
      items.add(index, aItem);
      wasAdded = true;
    }
    return wasAdded;
  }

  public boolean addOrMoveItemAt(Items aItem, int index)
  {
    boolean wasAdded = false;
    if(items.contains(aItem))
    {
      if(index < 0 ) { index = 0; }
      if(index > numberOfItems()) { index = numberOfItems() - 1; }
      items.remove(aItem);
      items.add(index, aItem);
      wasAdded = true;
    } 
    else 
    {
      wasAdded = addItemAt(aItem, index);
    }
    return wasAdded;
  }

  public void delete()
  {
    for(int i=items.size(); i > 0; i--)
    {
      Items aItem = items.get(i - 1);
      aItem.delete();
    }
  }


  public String toString()
  {
    return super.toString() + "["+
            "amountItems" + ":" + getAmountItems()+ "," +
            "name" + ":" + getName()+ "," +
            "address" + ":" + getAddress()+ "]";
  }
}