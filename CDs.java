/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.1.5123.801ad7a04 modeling language!*/


import java.sql.Date;

// line 14 "model.ump"
// line 55 "model.ump"
public class CDs extends Items
{

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public CDs(boolean aAvailable, Date aAvailableDate, boolean aBorrowable, boolean aInInventory, String aName, int aId, Library aLibrary)
  {
    super(aAvailable, aAvailableDate, aBorrowable, aInInventory, aName, aId, aLibrary);
  }

  //------------------------
  // INTERFACE
  //------------------------

  public void delete()
  {
    super.delete();
  }

}