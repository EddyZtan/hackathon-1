/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.1.5123.801ad7a04 modeling language!*/


import java.sql.Date;

// line 29 "model.ump"
// line 70 "model.ump"
public class Items
{

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //Items Attributes
  private boolean available;
  private Date availableDate;
  private boolean borrowable;
  private boolean inInventory;
  private String name;
  private int id;

  //Items Associations
  private Library library;

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Items(boolean aAvailable, Date aAvailableDate, boolean aBorrowable, boolean aInInventory, String aName, int aId, Library aLibrary)
  {
    available = aAvailable;
    availableDate = aAvailableDate;
    borrowable = aBorrowable;
    inInventory = aInInventory;
    name = aName;
    id = aId;
    boolean didAddLibrary = setLibrary(aLibrary);
    if (!didAddLibrary)
    {
      throw new RuntimeException("Unable to create item due to library. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
    }
  }

  //------------------------
  // INTERFACE
  //------------------------

  public boolean setAvailable(boolean aAvailable)
  {
    boolean wasSet = false;
    available = aAvailable;
    wasSet = true;
    return wasSet;
  }

  public boolean setAvailableDate(Date aAvailableDate)
  {
    boolean wasSet = false;
    availableDate = aAvailableDate;
    wasSet = true;
    return wasSet;
  }

  public boolean setBorrowable(boolean aBorrowable)
  {
    boolean wasSet = false;
    borrowable = aBorrowable;
    wasSet = true;
    return wasSet;
  }

  public boolean setInInventory(boolean aInInventory)
  {
    boolean wasSet = false;
    inInventory = aInInventory;
    wasSet = true;
    return wasSet;
  }

  public boolean setName(String aName)
  {
    boolean wasSet = false;
    name = aName;
    wasSet = true;
    return wasSet;
  }

  public boolean setId(int aId)
  {
    boolean wasSet = false;
    id = aId;
    wasSet = true;
    return wasSet;
  }

  public boolean getAvailable()
  {
    return available;
  }

  public Date getAvailableDate()
  {
    return availableDate;
  }

  public boolean getBorrowable()
  {
    return borrowable;
  }

  public boolean getInInventory()
  {
    return inInventory;
  }

  public String getName()
  {
    return name;
  }

  public int getId()
  {
    return id;
  }
  /* Code from template association_GetOne */
  public Library getLibrary()
  {
    return library;
  }
  /* Code from template association_SetOneToMany */
  public boolean setLibrary(Library aLibrary)
  {
    boolean wasSet = false;
    if (aLibrary == null)
    {
      return wasSet;
    }

    Library existingLibrary = library;
    library = aLibrary;
    if (existingLibrary != null && !existingLibrary.equals(aLibrary))
    {
      existingLibrary.removeItem(this);
    }
    library.addItem(this);
    wasSet = true;
    return wasSet;
  }

  public void delete()
  {
    Library placeholderLibrary = library;
    this.library = null;
    if(placeholderLibrary != null)
    {
      placeholderLibrary.removeItem(this);
    }
  }


  public String toString()
  {
    return super.toString() + "["+
            "available" + ":" + getAvailable()+ "," +
            "borrowable" + ":" + getBorrowable()+ "," +
            "inInventory" + ":" + getInInventory()+ "," +
            "name" + ":" + getName()+ "," +
            "id" + ":" + getId()+ "]" + System.getProperties().getProperty("line.separator") +
            "  " + "availableDate" + "=" + (getAvailableDate() != null ? !getAvailableDate().equals(this)  ? getAvailableDate().toString().replaceAll("  ","    ") : "this" : "null") + System.getProperties().getProperty("line.separator") +
            "  " + "library = "+(getLibrary()!=null?Integer.toHexString(System.identityHashCode(getLibrary())):"null");
  }
}