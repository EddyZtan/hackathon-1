/*PLEASE DO NOT EDIT THIS CODE*/
/*This code was generated using the UMPLE 1.30.1.5123.801ad7a04 modeling language!*/


import java.sql.Date;

// line 24 "model.ump"
// line 65 "model.ump"
public class Periodicals extends Items
{

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Periodicals(boolean aAvailable, Date aAvailableDate, boolean aBorrowable, boolean aInInventory, String aName, int aId, Library aLibrary)
  {
    super(aAvailable, aAvailableDate, aBorrowable, aInInventory, aName, aId, aLibrary);
  }

  //------------------------
  // INTERFACE
  //------------------------

  public void delete()
  {
    super.delete();
  }

}